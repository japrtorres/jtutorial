package com.elavon.jtutorial.ex4;

public class TwelveDaysOfChristmas {

	public static String christmasDay = null, dayOfTheMonth = null;
	public static int day;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TwelveDaysOfChristmas.printLyrics();
		    
	}
	
public static void printLyrics() {
	for (day = 1; day <= 12 ; day++)	{
		switch(day)	{
			case 1: 
				dayOfTheMonth = "first";
				break;
			case 2: 
				dayOfTheMonth = "second";
				break;
			case 3: 
				dayOfTheMonth = "third";
				break;
			case 4: 
				dayOfTheMonth = "fourth";
				break;
			case 5: 
				dayOfTheMonth = "fifth";
				break;
			case 6: 
				dayOfTheMonth = "sixth";
				break;
			case 7: 
				dayOfTheMonth = "seventh";
				break;
			case 8: 
				dayOfTheMonth = "eigth";
				break;
			case 9: 
				dayOfTheMonth = "ninth";
				break;
			case 10: 
				dayOfTheMonth = "tenth";
				break;
			case 11: 
				dayOfTheMonth = "eleventh";
				break;
			case 12: 
				dayOfTheMonth = "twelfth";
				break;
			default:
					break;
		}
			
		System.out.println("On the " + dayOfTheMonth + " day of Christmas");
		System.out.println("my true love sent to me,");
		printGifts(day);
		System.out.println("");
	}
}
	
public static void printGifts(int currentDay){
	if(currentDay == 1)
		System.out.println("a Partridge in a Pear Tree");
	else
		for (; currentDay >= 0 ; currentDay--) {
			switch(currentDay)	{
				case 1: 
					System.out.println("and a Partridge in a Pear Tree");
					break;
				case 2: 
					System.out.println("2 Turtle Doves");
					break;
				case 3: 
					System.out.println("3 French Hens");
					break;
				case 4: 
					System.out.println("4 Calling Birds");
					break;
				case 5: 
					System.out.println("5 Golden Rings");
					break;
				case 6: 
					System.out.println("6 Geese a Laying");
					break;
				case 7: 
					System.out.println("7 Swans a Swimming");
					break;
				case 8: 
					System.out.println("8 Maids a Milking");
					break;
				case 9: 
					System.out.println("9 Ladies Dancing");
					break;
				case 10:
					System.out.println("10 Lords a Leaping");
					break;
				case 11: 
					System.out.println("11 Pipers Piping");
					break;
				case 12: 
					System.out.println("12 Drummers Drumming");
					break;
				default:
						break;
			}
		}
	}
}
