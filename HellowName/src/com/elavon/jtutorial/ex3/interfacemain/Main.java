package com.elavon.jtutorial.ex3.interfacemain;

import com.elavon.jtutorial.ex3.interfaceimpl.AwesomeCalculatorImpl;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AwesomeCalculatorImpl ACImpl = new AwesomeCalculatorImpl();
		System.out.println(ACImpl.getSum(2,2));
		
		int sum = 0;
	    sum = ACImpl.getSum(new int[]{10,12,33});
		System.out.println("Get Sum using var args result: " + sum);
		
		System.out.println(ACImpl.getDifference(10, 8));
		System.out.println(ACImpl.getProduct(5, 5));
		
		double product = 0;
		product = ACImpl.getProduct(new double[]{10,12,33});
		System.out.println("Get Product using var args result: " + product);
		
		System.out.println(ACImpl.getQuotientAndRemainder(10, 3));
		System.out.println(ACImpl.toCelsius(100));
		System.out.println(ACImpl.toFahrenheit(30));
		System.out.println(ACImpl.toKilogram(165.346696635));
		System.out.println(ACImpl.toPound(75));
		System.out.println(ACImpl.isPalindrome("Racecar"));

	}

}
