package com.elavon.jtutorial.ex3.interfaceimpl;

import com.elavon.jtutorial.ex3.interfacetraining.AwesomeCalculator;

public class AwesomeCalculatorImpl implements AwesomeCalculator{
	
	public int getSum(int augend, int addend) {
		 System.out.print("Get Sum result of " + augend + " and " + addend + ": ");
		 return augend + addend;
	}

	public int getSum(int... summands) {
		int sum, i;
	    sum = 0;
	    for(i = 0; i< summands.length; i++) {
	       sum += summands[i];
	    }
	    return sum;
	}
	
	public double getDifference(double minuend, double subtrahend) {
		// TODO Auto-generated method stub
		 System.out.print("Get Difference result of " + minuend + " and " + subtrahend + ": ");
		 return minuend - subtrahend;
	}

	public double getProduct(double multiplicand, double multiplier) {
		// TODO Auto-generated method stub
		 System.out.print("Get Product result of " + multiplicand + " and " + multiplier + ": ");
		 return multiplicand * multiplier;
	}
	
	public double getProduct(double... factors) {
		int product, i;
		product = 0;
	    for(i = 0; i< factors.length; i++) {
	    	product *= factors[i];
	    }
	    return product;
	}

	public String getQuotientAndRemainder(double dividend, double divisor) {
		// TODO Auto-generated method stub
		 System.out.print("Get Quotient and Remainder result of " + dividend + " and " + divisor + ": ");
		 return "Quotient: " + (int)(dividend/divisor) + " Remainder: " + (int)(dividend%divisor);
	}

	public double toCelsius(int fahrenheit) {
		// TODO Auto-generated method stub
		 System.out.print("Get Celsius conversion of " + fahrenheit + ": ");
		 return (fahrenheit-32) /1.8;
	}

	public double toFahrenheit(int celsius) {
		// TODO Auto-generated method stub
		 System.out.print("Get Fahrenheit conversion of " + celsius + ": ");
		 return (celsius*1.8) + 32;
	}

	public double toKilogram(double lbs) {
		// TODO Auto-generated method stub
		 System.out.print("Get Kilogram conversion of " + lbs + ": ");
		 return lbs * 0.454;
	}

	public double toPound(double kg) {
		// TODO Auto-generated method stub
		 System.out.print("Get Pound conversion of " + kg + ": ");
		 return kg * 2.2;
	}

	public boolean isPalindrome(String str) {
		// TODO Auto-generated method stub
	    String reverse = "";
		int length = str.length();
		 
	    for ( int i = length - 1; i >= 0; i-- )
	       reverse = reverse + str.charAt(i);

	    System.out.print("Get Palindrome validation of " + str.toLowerCase() + " and " + reverse.toLowerCase() + ": ");
	    if (str.toLowerCase().equals(reverse.toLowerCase()))
	       return true;
	    else
	       return false;
		
		
	}
}