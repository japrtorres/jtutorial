package com.elavon.jtutorial.ex2;

public class Television {

	public static String brandName;
	public static String modelName;
	public static Boolean powerOn;
	public static int channel;
	public static int volume;
	
	public Television(String brand, String model){
		brandName = brand;
		modelName = model;
		powerOn = false;
		channel = 0;
		volume = 5;
	}
	
	public static void turnOn(){
		powerOn = true;
	}

	public static void turnOff(){
		powerOn = false;
	}
	
	public static void channelUp(){
		channel += 1;
	}

	public static void channelDown(){
		channel -= 1;
	}
	
	public static void volumeUp(){
		volume += 1;
	}

	public static void volumeDown(){
		volume -= 1;
	}
	
	//@Override
	public String toString() {
		return brandName + " " + modelName + " [ on:" + powerOn + ", channel: " + channel + ", volume: " + volume + " ] ";
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Television tv = new Television("Sony", "Alpha");
		
		tv.turnOn();
		tv.channelUp();
		tv.channelUp();
		tv.channelUp();
		tv.channelUp();
		tv.channelUp();

		tv.channelDown();
		
		tv.volumeDown();
		tv.volumeDown();
		tv.volumeDown();
		

		tv.volumeUp();
		
		tv.turnOff();
		
	    System.out.println("Television Class Output: " + tv);


	}

}