package com.elavon.jtutorial.ex2;


public class SmartTV extends ColoredTV{

	public SmartTV(String brand, String model) {
		super(brand, model);
		// TODO Auto-generated constructor stub
	}
	
	private static boolean netConnection = false;
    private static NetworkConnectionImpl NCImpl = new NetworkConnectionImpl();
	
	private static void connectToNetwork(String NetworkName){
		netConnection = NCImpl.connect(NetworkName);
	}
	
	private static boolean hasNetworkConnection(){
		return NCImpl.connectionstatus();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SmartTV samsungTV = new SmartTV("SAMSUNG", "SMTV1");
	    System.out.println("SmartTV Class Output 1: " + samsungTV);
	    mute();

	    brightnessUp();
	    brightnessUp();
	    brightnessUp();
	    brightnessDown();
	    brightnessDown();
	    brightnessDown();
	    brightnessDown();
	    brightnessDown();
	    contrastUp();
	    contrastUp();
	    contrastUp();
	    contrastDown();
	    contrastDown();
	    contrastDown();
	    contrastDown();
	    contrastDown();
	    pictureUp();
	    pictureUp();
	    pictureUp();
	    pictureDown();
	    channelUp();
	    channelUp();
	    

	    System.out.println("SmartTV Class Output 2: " + samsungTV);

	    System.out.println("Network Connection Status: " + hasNetworkConnection());
	    System.out.println("Connect to Network Name: What Happened?");
	    connectToNetwork("What Happened?");
	    if ( netConnection == true){
	    	System.out.println("Establish Network Conenction Successful");
	    }
	    else
	    	System.out.println("Establish Network Conenction Failed");
	    
	    System.out.println("Network Connection Status: " + hasNetworkConnection());
	    
	    
	    
	}

}
