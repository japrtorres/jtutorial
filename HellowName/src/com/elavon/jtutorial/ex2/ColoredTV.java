package com.elavon.jtutorial.ex2;

public class ColoredTV extends Television {

	public ColoredTV(String brand, String model) {
		super(brand, model);
		this.brightness = 50;
		this.contrast = 50;
		this.picture = 50;
	}

	private static int brightness;
	private static int contrast;
	private static int picture;
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Television bnwTV;
		ColoredTV sonyTV;
		
		bnwTV = new ColoredTV("Admiral","A1");
		sonyTV = new ColoredTV("SONY","S1");
		

	    System.out.println("ColoredTV Class Output 1: " + bnwTV);
	    System.out.println("ColoredTV Class Output 2: " + sonyTV);
	    
	    brightnessUp();
	    
	    ColoredTV sharpTV = new ColoredTV("SHARP","SH1");
	    System.out.println("ColoredTV Class Output 3: " + sharpTV);
	    
	    mute();
	    

	    brightnessDown();
	    brightnessUp();
	    contrastUp();
	    contrastUp();
	    contrastDown();
	    pictureUp();
	    pictureUp();
	    pictureUp();
	    pictureDown();  
	    switchToChannel(1);
	    System.out.println("ColoredTV Class Output 1: " + bnwTV);
	    
	    

	    brightnessDown();
	    brightnessUp();
	    brightnessUp();
	    contrastUp();
	    contrastUp();
	    contrastUp();
	    contrastDown();
	    pictureUp();
	    pictureUp();
	    pictureUp();
	    pictureUp();
	    pictureDown();  
	    switchToChannel(2);
	    volumeUp();
	    volumeUp();
	    System.out.println("ColoredTV Class Output 2: " + sonyTV);
	    

	    brightnessDown();
	    brightnessUp();
	    brightnessUp();
	    brightnessUp();
	    contrastUp();
	    contrastUp();
	    contrastUp();
	    contrastUp();
	    contrastDown();
	    pictureUp();
	    pictureUp();
	    pictureUp();
	    pictureUp();
	    pictureUp();
	    pictureDown(); 
	    switchToChannel(3);
	    volumeUp();
	    volumeUp();
	    volumeUp();
	    mute();
	    System.out.println("ColoredTV Class Output 3: " + sharpTV);
		
	}
	
	public static void brightnessUp(){
		brightness += 1;
	}

	public static void brightnessDown(){
		brightness -= 1;
	}
	
	public static void contrastUp(){
		contrast += 1;
	}

	public static void contrastDown(){
		contrast -= 1;
	}
	
	public static void pictureUp(){
		picture += 1;
	}

	public static void pictureDown(){
		picture -= 1;
	}

	public static void switchToChannel(int channel1){
		channel = channel1;
	}

	public static void mute(){
		volume = 0;
	}
	
	//@Override
	public String toString() {
		return brandName + " " + modelName + " [ on:" + powerOn + ", channel: " + channel + ", volume: " + volume + " ] [ b:" + brightness + ", c:" + contrast + ", p:" + picture + "]";
	}


}